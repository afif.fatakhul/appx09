package afifudin.fatakhul.appx09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DB(context: Context): SQLiteOpenHelper(context,DB_Name,null,DB_ver) {

    companion object{
        val DB_Name = "playlist"
        val DB_ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val lagu = "create table lagu (IDlagu varchar(30) primary key , IDcover varchar(30) not null, judul text not null)"
        val insertlagu = "insert into lagu values('0x7f0c0000','0x7f06005f','Dont Start Now - Dua Lipa'),('0x7f0c0001','0x7f060060','I`ll Be There - jess glynne'),('0x7f0c0002','0x7f060061','Lose you to love me - Selena Gomez')"

        db?.execSQL(lagu)
        db?.execSQL(insertlagu)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("Not yet implemented")
    }

}